groups:
- name: roles/float-infra-prometheus/templates/rules/alerts_base.conf
  rules:

  # HostUnreachable is used as a gate for most other host-based pages
  # (via inhibit rules in the alertmanager configuration).
  - alert: HostUnreachable
    expr: host_reachable == 0
    for: 1m
    labels:
      severity: warn
    annotations:
      summary: Host {{ $labels.host }} is down
      description: 'Host {{ $labels.host }} is unreachable (icmp ping).'

  - alert: NeedsReboot
    expr: node_reboot_required > 0
    for: 30m
    labels:
      severity: warn
    annotations:
      summary: 'Host {{ $labels.host }} needs to reboot'
      description: 'Host {{ $labels.host }} needs to reboot, there are pending kernel upgrades.'

  - alert: Reboot
    expr: os_uptime < 900
    for: 5m
    labels:
      severity: warn
    annotations:
      summary: 'Reboot on {{ $labels.host }}'
      description: 'The host {{ $labels.host }} has just rebooted. Hopefully this was expected.'

  - alert: JobDown
    expr: up < 1
    for: 5m
    labels:
      severity: warn
      scope: host
    annotations:
      summary: 'Job {{ $labels.job }}@{{ $labels.host }} is down'
      description: 'Job {{ $labels.job }} on {{ $labels.host }} has been down
        for more than 5 minutes. If this is a prober job, then the alert refers
        to the prometheus-blackbox-exporter service itself.'

  - alert: JobDown
    expr: job:up:ratio < 1
    for: 5m
    labels:
      severity: warn
      scope: global
    annotations:
      summary: 'Job {{ $labels.job }} has degraded redundancy'
      description: 'Job {{ $labels.job }} is running with slightly degraded
        redundancy ({{ $value }}) and may eventually be at risk.'

  - alert: JobDown
    expr: job:up:ratio < 0.5
    for: 5m
    labels:
      severity: page
      scope: global
    annotations:
      summary: 'Job {{ $labels.job }} is down globally'
      description: 'Job {{ $labels.job }} is down globally (availability {{ $value }}).'
      runbook: '[[ alert_runbook_fmt | format("JobDown") ]]'

  - alert: ProbeFailure
    expr: target:probe_success:ratio{probe!="ping",probeset!="service"} < 0.5
    for: 5m
    labels:
      severity: page
      scope: host
    annotations:
      summary: 'Probe {{ $labels.probe }}@{{ $labels.target }} is failing'
      description: 'Probe {{ $labels.probe }} ({{ $labels.zone }}) is failing
        for target {{ $labels.target }} (success ratio {{ $value }}).'
      runbook: '[[ alert_runbook_fmt | format("ProbeFailure") ]]'

  - alert: ProbeFailure
    expr: probe:probe_success:ratio{probe!="ping",probeset!="service"} < 0.5
    for: 5m
    labels:
      severity: page
      scope: global
    annotations:
      summary: 'Probe {{ $labels.probe }} is failing globally'
      description: 'Probe {{ $labels.probe }} ({{ $labels.zone }}) is failing
        globally (success ratio {{ $value }}).'
      runbook: '[[ alert_runbook_fmt | format("ProbeFailure") ]]'

  - alert: CronJobFailure
    expr: cronjob_ok < 1 and (time() - cronjob_last_success) > 259200
    for: 30m
    labels:
      severity: warn
      scope: host
    annotations:
      summary: 'Cron job {{$labels.cronjob}} on {{$labels.host}} has been failing consistently'
      description: 'The cron job "{{$labels.cronjob}}" on host {{$labels.host}} has been
        failing for the last 3 days. Check the logs for errors.'
      runbook: '[[ alert_runbook_fmt | format("CronJobFailure") ]]'
