{% for n in net_overlays %}
acl "net_{{ n.name }}" {
  {{ n.network }};
};
{% endfor %}

options {
  directory "/var/cache/bind";

{% if float_limit_bind_to_known_interfaces | default(False) %}
  listen-on {
    127.0.0.1;
{% for h in groups['frontend'] | sort %}
    {{ hostvars[h]['ip'] }};
{% for n in net_overlays | sort if ('ip_' + n.name) in hostvars[h] %}
    {{ hostvars[h]['ip_' + n.name] }};
{% endfor %}
{% endfor %}
  };
  listen-on-v6 {
    ::1;
{% for h in groups['frontend'] | sort if 'ip6' in hostvars[h] %}
    {{ hostvars[h]['ip6'] }};
{% endfor %}
  };
{% else %}
  listen-on { any; };
  listen-on-v6 { any; };
{% endif %}

  dnssec-enable yes;
  dnssec-validation auto;

  notify no;
  rrset-order { order random; };

  // Stay away from UDP size problems.
  edns-udp-size 512;

  // Conform to RFC1035.
  auth-nxdomain no;

  allow-transfer { none; };
  allow-query {
    localhost;
{% for n in net_overlays %}
    net_{{ n.name }};
{% endfor %}
  };
};

logging {
  category lame-servers { null; };
  category edns-disabled { null; };
};

statistics-channels {
  inet 127.0.0.1 port 8053 allow { localhost; };
};

{% if acme_tsig_key is defined %}
key acme {
  algorithm "{{ acme_tsig_key.algo | lower }}";
  secret "{{ acme_tsig_key.public }}";
};
{% endif %}
